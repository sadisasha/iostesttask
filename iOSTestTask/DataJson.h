//
//  DataJson.h
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Model.h"

@interface DataJson : NSObject

@property (nonatomic, strong, getter=dataSource) NSArray *dataSource;

@end
