//
//  Model.m
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "Model.h"

#pragma mark - Implementation Item

@implementation Item

- (instancetype)initWithId:(NSInteger)_id withName:(NSString *)name {
    
    self = [super init];
    
    if (self) {
        
        self._id = _id;
        self.name = name;
    }
    
    return self;
}

@end

#pragma mark - Implementation Group model

@implementation Group

- (instancetype)initWithId:(NSInteger)_id withName:(NSString *)name {
    
    self = [super initWithId:_id withName:name];
    
    if (self) {
        
        self.isOpened = NO;
        self.items = [NSMutableArray new];
    }
    
    return self;
}

@end