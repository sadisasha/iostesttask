//
//  UIView+Rotate.h
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Rotate)

- (void)rotateClockwise:(BOOL)clockwise;

@end
