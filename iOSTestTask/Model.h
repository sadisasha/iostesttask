//
//  Model.h
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Interface Item

@interface Item : NSObject

@property NSInteger _id;
@property (strong) NSString *name;

- (instancetype)initWithId:(NSInteger)_id withName:(NSString *)name;

@end

#pragma mark - Interface Group model

@interface Group : Item

@property BOOL isOpened;
@property (strong) NSMutableArray *items;

@end