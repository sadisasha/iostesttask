//
//  DataJson.m
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "DataJson.h"

@implementation DataJson

@synthesize dataSource = _dataSource;

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        _dataSource = [self parseDataJsonFile];
    }
    
    return self;
}

- (id)dataSource {
    
    return _dataSource;
}

- (NSArray *)parseDataJsonFile {

    NSMutableArray *groups = [NSMutableArray new];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"json"];

    if (!path) {
        
        return groups;
    }
    
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    
    if (jsonData) {
        
        NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
        
        if (error) {
            
            NSLog(@"Json serialization with error: %@", [error localizedDescription]);
            
        } else if (dictionary) {

            for (NSDictionary *group in [dictionary objectForKey:@"groups"]) {
                
                if ([group objectForKey:@"id"] && [group objectForKey:@"name"]) {
                    
                    Group *model = [[Group alloc] initWithId:[[group objectForKey:@"id"] integerValue] withName:[group objectForKey:@"name"]];
                    [groups addObject:model];
                }
            }
            
            if ([groups count] > 0) {
                
                for (NSDictionary *item in [dictionary objectForKey:@"items"]) {
                    
                    if ([item objectForKey:@"group_id"]) {
                        
                        for (Group *group in groups) {
                            
                            if ([[item objectForKey:@"group_id"] integerValue] == group._id) {
                                
                                if ([item objectForKey:@"id"] && [item objectForKey:@"name"]) {
                                    
                                    Item *model = [[Item alloc] initWithId:[[item objectForKey:@"id"] integerValue] withName:[item objectForKey:@"name"]];
                                    [group.items addObject:model];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    return groups;
}

@end
