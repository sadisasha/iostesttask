//
//  GroupCell.m
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "GroupCell.h"

#import "UIColor+Hex.h"

#define kSelectionBackgroundColor [UIColor colorWithHex:0xbababa]

#define kGroupBackgroundColor [UIColor whiteColor]
#define kItemBackgroundColor  [UIColor colorWithHex:0xeeeeee]

@interface GroupCell ()

@property (strong, nonatomic) IBOutlet UIImageView *dropDownImageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leadingMarginConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *trailingMarginConstraint;

@end

@implementation GroupCell

@synthesize cellType = _cellType;

#pragma mark - Getters & Setters

- (void)setCellType:(CellType)cellType {
    
    _cellType = cellType;
    
    if (cellType == GroupType) {
      
        self.backgroundColor = kGroupBackgroundColor;
        
        self.leadingMarginConstraint.constant = 16.0;
        self.trailingMarginConstraint.constant = 48.0;
        
    } else if (cellType == ItemType) {
        
        self.backgroundColor = kItemBackgroundColor;
        
        self.leadingMarginConstraint.constant = 24.0;
        self.trailingMarginConstraint.constant = 16.0;
    }
}

- (void)setOpened:(BOOL)opened {

    [self.dropDownImageView setImage:[UIImage imageNamed:(opened) ? @"list-up" : @"list-down"]];
}

- (void)setDropDownHidden:(BOOL)hidden {
    
    [self.dropDownImageView setHidden:hidden];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        
        self.backgroundColor = kSelectionBackgroundColor;
        
    } else {
        
        if (_cellType == GroupType) {
            
            self.backgroundColor = kGroupBackgroundColor;
            
        } else if (_cellType == ItemType) {
            
            self.backgroundColor = kItemBackgroundColor;
        }
    }
}

#pragma mark - UIResponder methods

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    [self setSelected:YES];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    [self setSelected:NO];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    [self setSelected:NO];
}

@end
