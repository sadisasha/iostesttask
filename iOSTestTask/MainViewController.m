//
//  MainViewController.m
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "MainViewController.h"

#import "DataJson.h"
#import "GroupCell.h"
#import "UIView+Rotate.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property NSArray *objects;

@property (nonatomic) IBInspectable BOOL showEmptyGroup;

- (void)showAlertWithTitle:(NSString *)title withMessage:(NSString *)message;
- (void)onLongPressedAction:(UILongPressGestureRecognizer *)sender;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.showEmptyGroup) {
        
        self.objects = [[DataJson new] dataSource];
        
    } else {
        
        NSMutableArray *dataSource = [NSMutableArray new];
        
        for (Group *group in [[DataJson new] dataSource]) {
            
            if ([group.items count] > 0) {
                
                [dataSource addObject:group];
            }
        }
        
        self.objects = dataSource;
    }
    
    UILongPressGestureRecognizer *longPressed = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPressedAction:)];
    longPressed.minimumPressDuration = 1.0;
    
    [self.tableView addGestureRecognizer:longPressed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)showAlertWithTitle:(NSString *)title withMessage:(NSString *)message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)onLongPressedAction:(UILongPressGestureRecognizer *)sender {
    
    CGPoint pont = [sender locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pont];
    
    if (indexPath) {
        
        if (sender.state == UIGestureRecognizerStateEnded) {
            
            Group *group = [self.objects objectAtIndex:[indexPath section]];

            GroupCell *cell = (GroupCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            
            if ([group.items count] > 0 && [indexPath row] > 0) {

                [cell setCellType:ItemType];
                
                Item *item = [group.items objectAtIndex:[indexPath row] - 1];
                
                [self showAlertWithTitle:group.name withMessage:item.name];
            
            } else {
                
                [cell setCellType:GroupType];
                
                if (group._id % 2) {
                    
                    //counter-clockwise
                    [self.tableView rotateClockwise:NO];
                    
                } else {
                    
                    //clockwise
                    [self.tableView rotateClockwise:YES];
                }
            }
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [self.objects count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    Group *group = [self.objects objectAtIndex:section];
    
    if (group.isOpened) {
        
        return [group.items count] + 1;
        
    } else {
        
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"GroupCell";
    
    Group *group = [self.objects objectAtIndex:[indexPath section]];
    
    GroupCell *cell = (GroupCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {

        cell = [[GroupCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (group.isOpened) {
        
        if ([indexPath row] == 0) {
            
            cell.opened = YES;
            cell.cellType = GroupType;
            cell.dropDownHidden = [group.items count] > 0 ? NO : YES;
            
            [cell.title setText:[group name]];
            
        } else {
            
            cell.cellType = ItemType;
            cell.dropDownHidden = YES;
            
            Item *item = [group.items objectAtIndex:[indexPath row] - 1];
            [cell.title setText:item.name];

        }
     
    } else {
        
        cell.opened = NO;
        cell.cellType = GroupType;
        cell.dropDownHidden = [group.items count] > 0 ? NO : YES;
        
        [cell.title setText:[group name]];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Group *group = [self.objects objectAtIndex:[indexPath section]];
    
    if ([group.items count] > 0 && [indexPath row] == 0) {
        
        group.isOpened = !group.isOpened;
        
        [self.tableView reloadData];
    }
}

@end
