//
//  MainViewController.h
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface MainViewController : UITableViewController

@end
