//
//  GroupCell.h
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    GroupType,
    ItemType
} CellType;

@interface GroupCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title;

@property (nonatomic) BOOL dropDownHidden;
@property (nonatomic) CellType cellType;

@property (nonatomic) BOOL opened;

@end
