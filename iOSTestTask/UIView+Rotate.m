//
//  UIView+Rotate.m
//  iOSTestTask
//
//  Created by Aleksandr Sadikov on 21.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import "UIView+Rotate.h"

CGFloat const kDuration = 1.0;

@implementation UIView (Rotate)

- (void)rotateClockwise:(BOOL)clockwise {
    
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fromValue = clockwise ? [NSNumber numberWithFloat:0] : [NSNumber numberWithFloat:M_PI * 2.0];
    rotationAnimation.toValue = clockwise ? [NSNumber numberWithFloat:M_PI * 2.0] : [NSNumber numberWithFloat:0];
    rotationAnimation.duration = kDuration;
    rotationAnimation.repeatCount = 1.0;
    
    [self.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end
